# Alpine 3.11 contains Python 3.8, pyspark only supports Python up to 3.7
FROM alpine:3.10.4

LABEL maintainer="Jhon Fredy Cote <jf@coterey.com>"

# openjdk8: Java
# bash: Hadoop is not compatible with Alpine's `ash` shell
# coreutils: Spark launcher script relies on GNU implementation of `nice`
# procps: Hadoop needs GNU `ps` utility
# findutils: Spark needs GNU `find` to run jobs (weird but true)
# ncurses: so that you can run `yarn top`
# libc6-compat: https://issues.apache.org/jira/browse/SPARK-26995
# curl and unzip: download and extract Hadoop, Spark etc.
# lapack: Dependencies for numpy 1.18.3 y pandas 0.25.3
# postgresql-dev: Dependencies for psycopg2 postgres handler
# libffi-dev: Dependencies for flask-bcrypt
# musl-dev and gcc: General librarys for builds
RUN apk add --no-cache \
            openjdk8=~8.242 \
            bash=~5.0 \
            coreutils=~8.31 \
            procps=~3.3 \
            findutils=~4.6 \
            ncurses=~6.1 \
            libc6-compat=~1.1.22 \
    && apk add --no-cache --virtual delete-deps \
            curl=~7.66 \
            unzip=~6.0 \
    && ln -s /lib64/ld-linux-x86-64.so.2 /lib/ld-linux-x86-64.so.2

# https://github.com/hadolint/hadolint/wiki/DL4006
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Hadoop
ENV HADOOP_VERSION=3.2.0
ENV HADOOP_HOME /usr/hadoop

# Mirror
# http://archive.apache.org/dist/hadoop/common/hadoop-${HADOOP_VERSION}
# https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors/
RUN curl --progress-bar -L --retry 3 \
  "https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors/hadoop-${HADOOP_VERSION}.tar.gz" \
  | gunzip \
  | tar -x -C /usr/ \
 && mv "/usr/hadoop-${HADOOP_VERSION}" "${HADOOP_HOME}" \
 && rm -rf "${HADOOP_HOME}/share/doc" \
 && chown -R root:root "${HADOOP_HOME}"

# Spark
ENV SPARK_VERSION=2.4.5
ENV SPARK_PACKAGE "spark-${SPARK_VERSION}-bin-without-hadoop"
ENV SPARK_HOME /usr/spark

# Mirror
# https://archive.apache.org/dist/spark/spark-${SPARK_VERSION}
# https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors
RUN curl --progress-bar -L --retry 3 \
  "https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors/${SPARK_PACKAGE}.tgz" \
  | gunzip \
  | tar x -C /usr/ \
 && mv "/usr/${SPARK_PACKAGE}" "${SPARK_HOME}" \
 && chown -R root:root "${SPARK_HOME}"

# PySpark - comment out if you don't want it in order to save image space
RUN apk add --no-cache \
    'python3=~3.7' \
    'python3-dev=~3.7' \
 && ln -s /usr/bin/python3 /usr/bin/python

# Common settings
ENV JAVA_HOME "/usr/lib/jvm/java-1.8-openjdk"
ENV PATH="${PATH}:${JAVA_HOME}/bin"
# http://blog.stuart.axelbrooke.com/python-3-on-spark-return-of-the-pythonhashseed
ENV PYTHONHASHSEED 0
ENV PYTHONIOENCODING UTF-8
ENV PIP_DISABLE_PIP_VERSION_CHECK 1

# Hadoop setup
ENV PATH="${PATH}:${HADOOP_HOME}/bin"
ENV HDFS_NAMENODE_USER="root"
ENV HDFS_DATANODE_USER="root"
ENV HDFS_SECONDARYNAMENODE_USER="root"
ENV YARN_RESOURCEMANAGER_USER="root"
ENV YARN_NODEMANAGER_USER="root"
ENV LD_LIBRARY_PATH="${HADOOP_HOME}/lib/native:${LD_LIBRARY_PATH}"
ENV HADOOP_CONF_DIR="${HADOOP_HOME}/etc/hadoop"
ENV HADOOP_LOG_DIR="${HADOOP_HOME}/logs"

# For S3 to work. Without this line you'll get "Class org.apache.hadoop.fs.s3a.S3AFileSystem not found" exception when accessing S3 from Hadoop
ENV HADOOP_CLASSPATH="${HADOOP_HOME}/share/hadoop/tools/lib/*"

# Hadoop JVM crashes on Alpine when it tries to load native libraries.
# Solution? Delete those altogether.
# Alternatively, you can try and compile them
# https://hadoop.apache.org/docs/stable/hadoop-project-dist/hadoop-common/NativeLibraries.html
RUN mkdir "${HADOOP_LOG_DIR}"  \
 && rm -rf "${HADOOP_HOME}/lib/native"

# Spark setup
ENV PATH="${PATH}:${SPARK_HOME}/bin"
ENV SPARK_CONF_DIR="${SPARK_HOME}/conf"
ENV SPARK_LOG_DIR="${SPARK_HOME}/logs"
ENV SPARK_DIST_CLASSPATH="${HADOOP_CONF_DIR}:${HADOOP_HOME}/share/hadoop/tools/lib/*:${HADOOP_HOME}/share/hadoop/common/lib/*:${HADOOP_HOME}/share/hadoop/common/*:${HADOOP_HOME}/share/hadoop/hdfs:${HADOOP_HOME}/share/hadoop/hdfs/lib/*:${HADOOP_HOME}/share/hadoop/hdfs/*:${HADOOP_HOME}/share/hadoop/mapreduce/lib/*:${HADOOP_HOME}/share/hadoop/mapreduce/*:${HADOOP_HOME}/share/hadoop/yarn:${HADOOP_HOME}/share/hadoop/yarn/lib/*:${HADOOP_HOME}/share/hadoop/yarn/*"

# Spark java ODBC
RUN curl --progress-bar -L \
    "https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/jars/ojdbc7.jar" \
    --output "${SPARK_HOME}/jars/ojdbc7.jar"

# Clean up
RUN rm -rf "${SPARK_HOME}/examples/src"

# Livy

# Mirror
# https://downloads.apache.org/incubator/livy/0.7.0-incubating/
# https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors/
RUN wget \
  "https://gitlab.com/zafhiel/spark-hadoop/-/raw/master/mirrors/apache-livy-0.7.0-incubating-bin.zip" \
 && unzip apache-livy-0.7.0-incubating-bin -d /usr/ \
 && mv "/usr/apache-livy-0.7.0-incubating-bin" "/usr/livy" \
 && mkdir /usr/livy/logs \
 && chown -R root:root "/usr/livy"

# For inscrutable reasons, Spark distribution doesn't include spark-hive.jar
# Livy attempts to load it though, and will throw
# java.lang.ClassNotFoundException: org.apache.spark.sql.hive.HiveContext
ARG SCALA_VERSION=2.11
RUN curl --progress-bar -L \
    "https://repo1.maven.org/maven2/org/apache/spark/spark-hive_${SCALA_VERSION}/${SPARK_VERSION}/spark-hive_${SCALA_VERSION}-${SPARK_VERSION}.jar" \
    --output "${SPARK_HOME}/jars/spark-hive_${SCALA_VERSION}-${SPARK_VERSION}.jar"

# pySpark
ENV PYTHONPATH="/usr/spark/python/lib/py4j-0.10.7-src.zip:/usr/spark/python:/usr/spark:/usr/lib/python37.zip:/usr/lib/python3.7:/usr/lib/python3.7/lib-dynload:/usr/lib/python3.7/site-packages"

RUN apk del delete-deps

ENTRYPOINT /usr/livy/bin/livy-server